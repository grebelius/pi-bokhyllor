export const populate = (first = 1, last = 10) => {
	if (last <= first) {
		console.error(
			'Make sure first and last arguments are integers so that first < last and both ar grater than or equal to 0'
		);
		return;
	}
	let result = [];
	for (let i = first; i < last + 1; i++) {
		result.push(i);
	}
	console.log(result);
	return result;
};
