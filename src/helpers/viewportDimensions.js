import { useState, useLayoutEffect } from "react";

const getHeight = () => window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
const getWidth = () => window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

function useCurrentHeight() {
	let [height, setHeight] = useState(getHeight());

	useLayoutEffect(() => {
		let timeoutId = null;
		const resizeListener = () => {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(() => {
				setHeight(getHeight());
			}, 150);
		};
		window.addEventListener("resize", resizeListener);

		return () => {
			window.removeEventListener("resize", resizeListener);
		};
	}, []);

	return height;
}
function useCurrentWidth() {
	let [width, setWidth] = useState(getWidth());

	useLayoutEffect(() => {
		let timeoutId = null;
		const resizeListener = () => {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(() => setWidth(getWidth()), 150);
		};
		window.addEventListener("resize", resizeListener);

		return () => {
			window.removeEventListener("resize", resizeListener);
		};
	}, []);

	return width;
}

export { useCurrentHeight, useCurrentWidth };

export default function useCurrentViewportDimensions() {
	let [width, setWidth] = useState(getWidth());
	let [height, setHeight] = useState(getHeight());

	useLayoutEffect(() => {
		let timeoutId = null;
		const resizeListener = () => {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(() => {
				setWidth(getWidth());
				setHeight(getHeight());
			}, 150);
		};
		window.addEventListener("resize", resizeListener);

		return () => {
			window.removeEventListener("resize", resizeListener);
		};
	}, []);

	return { viewportWidth: width, viewportHeight: height };
}
