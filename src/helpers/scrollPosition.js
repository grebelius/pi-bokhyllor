import { useState, useLayoutEffect } from 'react';

export const useWindowScrollY = () => {
	let [scrollY, setScrollY] = useState(0);

	useLayoutEffect(() => {
		let timeoutId = null;
		const scrollListener = () => {
			clearTimeout(timeoutId);
			timeoutId = setTimeout(() => {
				setScrollY(window.scrollY);
			}, 20);
		};

		window.addEventListener('scroll', scrollListener);

		return () => {
			window.removeEventListener('scroll', scrollListener);
		};
	}, []);
	return scrollY;
};
