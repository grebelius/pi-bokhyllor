import React from "react";
import bookData from "./bookData";

const Label = ({ currentRightmostCard }) => {
	return <div className="label">{bookData.label(currentRightmostCard)}</div>;
};

export default Label;
