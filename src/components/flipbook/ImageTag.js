import React, { useState } from "react";

const PictureTag = ({ vol, page, download }) => {
	// let filePath = `one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-1.pdf`;
	// if (page !== 2 && page !== 3 && page !== 390) {
	// 	filePath = `one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-${vol}.pdf`;
	// }

	let [isImageLoaded, setIsImageLoaded] = useState(false);

	return download ? (
		<img
			style={isImageLoaded ? {} : { opacity: 0 }}
			className="picture"
			src={`https://static.grebelius.se/${vol}-${page}.svg`}
			alt={`One Trillion Digits of Pi – Volume ${vol}, page ${page}`}
			onLoad={() => setIsImageLoaded(true)}
		/>
	) : (
		<></>
	);
};

export default PictureTag;
