const bookData = {
  title: "One Trillion Digits of Pi",
  get subtitle() {
    return `Volume ${this.volumeNumber} of 1,000,000`
  },
  volumesTotal: 1000000,
  volumeNumber: 2,
  author: "Lennart Grebelius",
  pages: {
    total: 391,
    preface: 3,
    main: 386,
    postface: 2,
  },
  label: function (currentCard) {
    if (currentCard === 1) {
      return "Volume cover"
    } else if (currentCard === 2) {
      return "ii—iii"
    } else if (currentCard === 196) {
      return "iv—v"
    } else if (currentCard > 196 || currentCard < 1) {
      return ""
    }
    return `${currentCard * 2 - 2 - this.pages.preface}—${
      currentCard * 2 - 1 - this.pages.preface
    }`
  },
  pageDimensions: {
    px: { width: 650, height: 1052 },
    mm: { width: 650, height: 1052 },
  },
  spineDimensions: {
    px: { width: 114, height: 1052 },
  },
  imageWidth: 640,
  pageFlipSpeedInMilliseconds: 1000,
}

export default bookData
