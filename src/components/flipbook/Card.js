import React from "react";

import ImageTag from "./ImageTag";

const Card = ({
	onClick,
	vol,
	cardNumber,
	position,
	totalNumberOfCards,
	bookData,
	flipDirection,
	downloadedImagesCallback,
	shouldDownloadImmediatly,
}) => {
	const frontPageNumber = cardNumber * 2 - 1;
	const backPageNumber = cardNumber * 2;

	const [shouldDownloadImage, setShouldDownloadImage] = React.useState(false);

	React.useEffect(() => {
		let lazyImageTimeout;
		if (shouldDownloadImmediatly) {
			setShouldDownloadImage(true);
		} else {
			lazyImageTimeout = setTimeout(() => {
				setShouldDownloadImage(true);
				downloadedImagesCallback(cardNumber);
			}, 500);
		}

		return () => {
			clearTimeout(lazyImageTimeout);
		};
	}, [shouldDownloadImmediatly, downloadedImagesCallback, cardNumber]);

	const cardInlineStyle = {
		transitionDuration: `${bookData.pageFlipSpeedInMilliseconds}ms`,
	};

	if (flipDirection === "forwards") {
		switch (position) {
			case 1:
				cardInlineStyle.zIndex = 910;
				cardInlineStyle.transform = "rotateY(-180deg)";
				break;
			case 2:
				cardInlineStyle.zIndex = 912;
				cardInlineStyle.transform = "rotateY(-180deg)";
				if (cardNumber !== 1) cardInlineStyle.boxShadow = "none";
				break;
			case 3:
				cardInlineStyle.zIndex = 911;
				cardInlineStyle.transform = "rotateY(0deg)";
				if (cardNumber !== totalNumberOfCards) cardInlineStyle.boxShadow = "none";
				break;
			case 4:
				cardInlineStyle.zIndex = 910;
				cardInlineStyle.transform = "rotateY(0deg)";
				break;
			default:
				console.error("Card components need a position parameter");
		}
	} else {
		switch (position) {
			case 1:
				cardInlineStyle.zIndex = 910;
				cardInlineStyle.transform = "rotateY(-180deg)";
				break;
			case 2:
				cardInlineStyle.zIndex = 911;
				cardInlineStyle.transform = "rotateY(-180deg)";
				if (cardNumber !== 1) cardInlineStyle.boxShadow = "none";
				break;
			case 3:
				cardInlineStyle.zIndex = 912;
				cardInlineStyle.transform = "rotateY(0deg)";
				if (cardNumber !== totalNumberOfCards) cardInlineStyle.boxShadow = "none";
				break;
			case 4:
				cardInlineStyle.zIndex = 910;
				cardInlineStyle.transform = "rotateY(0deg)";
				break;
			default:
				console.error("Card components need a position parameter");
		}
	}

	return cardNumber > 0 && cardNumber <= totalNumberOfCards ? (
		<div
			className="card"
			style={cardInlineStyle}
			onClick={(e) => {
				e.stopPropagation();
				onClick(cardNumber);
			}}
		>
			<div className="card-face">
				<ImageTag vol={vol} page={frontPageNumber} download={shouldDownloadImage} />
			</div>
			<div className="card-face back">
				{backPageNumber > bookData.pages.total ? (
					<div className="empty-page-cover-div" />
				) : (
					<ImageTag vol={vol} page={backPageNumber} download={shouldDownloadImage} />
				)}
			</div>
		</div>
	) : (
		<></>
	);
};

export default Card;
