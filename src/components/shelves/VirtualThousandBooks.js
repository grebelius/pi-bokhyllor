import React, { useState, useEffect, useLayoutEffect } from "react";
import { Link } from "wouter"
import { BookSpine } from "./BookSpine";
import Modal from "../Modal";
import "./VirtualThousandBooks.css";
import Flipbook from "../flipbook/Flipbook";

export const VirtualThousandBooks = ({ shelfNumber }) => {
	const volumeToFind = parseInt(window.location.hash.substr(1), 10);
	const firstBookInShelf = (shelfNumber - 1) * 1000;
	const minIndex = firstBookInShelf + 1;
	const maxIndex = firstBookInShelf + 1000;
	const totalItems = maxIndex - minIndex;
	const startIndex = minIndex;
	const bookWidth = 42;
	const bookHeight = 400;
	const marginWidth = 2;
	const itemWidth = bookWidth + marginWidth;
	const tolerance = 10;
	const toleranceWidth = tolerance * itemWidth;
	const leftPaddingMinWidth = 100;

	const totalWidth = (maxIndex - minIndex + 1) * itemWidth;
	const itemsAbove = startIndex - tolerance - minIndex;

	let [windowScrollOffset, setWindowScrollOffset] = useState(0);
	let [viewportWidth, setViewportWidth] = useState(window.innerWidth);
	let [numberOfBufferedItems, setNumberOfBufferedItems] = useState(
		Math.ceil(viewportWidth / itemWidth) + 2 * tolerance
	);
	let [leftPaddingWidth, setLeftPaddingWidth] = useState(leftPaddingMinWidth + itemsAbove * itemWidth);
	let [data, setData] = useState([]);

	useEffect(() => {
		if (!volumeToFind) return;
		const scrollToOffset = (volumeToFind - firstBookInShelf - 1) * itemWidth - viewportWidth / 3;
		window.scrollTo({
			top: 0,
			left: scrollToOffset,
			behavior: "smooth",
		});
	}, [itemWidth, viewportWidth, firstBookInShelf, volumeToFind]);

	useLayoutEffect(() => {
		let resizeTimeoutId = null;
		const deferResizeListenerTime = 150;

		const windowResizeListener = () => {
			clearTimeout(resizeTimeoutId);
			resizeTimeoutId = setTimeout(() => {
				setViewportWidth(window.innerWidth);
			}, deferResizeListenerTime);
		};
		const windowScrollListener = () => {
			setWindowScrollOffset(window.pageXOffset);
		};

		window.addEventListener("resize", windowResizeListener);
		window.addEventListener("scroll", windowScrollListener);

		return () => {
			window.removeEventListener("resize", windowResizeListener);
			window.removeEventListener("scroll", windowScrollListener);
		};
	}, []);

	// Trigger scroll animation when scrollToBook changes
	// useEffect(() => {
	// 	if (scrollToBook === null) return;
	// 	const scrollToTopOffset = (scrollToBook - 1) * itemWidth;
	// 	window.scrollTo({
	// 		top: scrollToTopOffset,
	// 		left: 0,
	// 		behavior: "smooth",
	// 	});
	// }, [scrollToBook, itemWidth]);
	// Update numberOfBufferedItems when viewportHeight changes
	useEffect(() => {
		setNumberOfBufferedItems(Math.ceil(viewportWidth / itemWidth) + 2 * tolerance);
	}, [viewportWidth, itemWidth, tolerance]);

	// Update stuff when windowScrollOffset changes
	useEffect(() => {
		const calculateData = (offset, limit) => {
			const data = [];
			const start = Math.max(minIndex, offset);
			const end = Math.min(offset + limit - 1, maxIndex);
			if (start <= end) {
				for (let i = start; i <= end; i++) {
					data.push(i);
				}
			}
			return data;
		};

		const index = minIndex + Math.floor((windowScrollOffset - toleranceWidth) / itemWidth);
		const data = calculateData(index, numberOfBufferedItems);
		const leftPadding = Math.max((index - minIndex) * itemWidth, leftPaddingMinWidth);

		setData(data);
		setLeftPaddingWidth(leftPadding);
	}, [windowScrollOffset, itemWidth, numberOfBufferedItems, toleranceWidth, totalWidth, minIndex, maxIndex]);

	// *******************************************************************************************
	// ******** MODAL ****************************************************************************
	// *******************************************************************************************

	let [isModalVisible, setIsModalVisible] = useState(false);
	let [modalBookNumber, setModalBookNumber] = useState(null);

	function openModal(bookNumber) {
		document.body.style.setProperty("overflow", "hidden");
		setModalBookNumber(bookNumber);
		setIsModalVisible(true);
	}
	function closeModal() {
		document.body.style.removeProperty("overflow");
		setIsModalVisible(false);
		setModalBookNumber(null);
	}
	let bookComponents = data.map((bookNumber) => (
		<BookSpine
			onClick={() => openModal(bookNumber)}
			key={bookNumber}
			bookNumber={bookNumber}
			hilight={volumeToFind === bookNumber}
		/>
	));
	return (
		<>
			<header>
				<div>
					<h1>
						<Link href="/">&laquo; Back</Link>
					</h1>
				</div>
			</header>
			<div
				style={{
					"--book-width": `${bookWidth}px`,
					"--book-height": `${bookHeight}px`,
					"--margin-width": `${marginWidth}px`,
					width: `${itemWidth * totalItems}px`,
				}}
				className="virtual-horizontal-scroller"
			>
				<div style={{ width: `${leftPaddingWidth}px` }} className="left-padding-div">
					{parseFloat(shelfNumber).toLocaleString("en")}
				</div>
				{bookComponents}
			</div>
			{isModalVisible && (
				<Modal onModalClose={closeModal}>
					<Modal.Body>
						<Flipbook vol={modalBookNumber} />
					</Modal.Body>
				</Modal>
			)}
		</>
	);
};
