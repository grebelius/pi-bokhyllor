import React from "react";

export function ThumbnailBookSpine() {
	return (
		<div className="thumbnail-book-spine">
			<div className="thumbnail-book-spine-title" />
			<div className="thumbnail-book-spine-subtitle" />
			<div className="thumbnail-book-spine-author" />
		</div>
	);
}
export function BookSpine({ bookNumber, onClick, hilight }) {
	return (
		<svg
			viewBox="0 0 50 456"
			className={`book-spine${hilight ? " hilight" : ""}`}
			onClick={() => {
				onClick(bookNumber);
			}}
			xmlns="http://www.w3.org/2000/svg"
		>
			<text x="-52" y="104" className="svg-title" fontSize="10" fontWeight="bold" transform="rotate(90 25 101)">
				ONE TRILLION DIGITS OF PI
			</text>
			<text x="-47" y="268" className="svg-subtitle" fontSize="10" fontStyle="italic" transform="rotate(90 25 265)">
				Volume <tspan className="svg-volume-number">{parseFloat(bookNumber).toLocaleString("en")}</tspan> of 1,000,000
			</text>
			<text x="-11" y="400" className="svg-author" fontSize="6" transform="rotate(90 25 397)">
				Lennart Grebelius
			</text>
		</svg>
	);
}
