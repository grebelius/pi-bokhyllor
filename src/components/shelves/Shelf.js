import React from "react";
import { Link } from "wouter"
import { ThumbnailBookSpine, BookSpine } from "./BookSpine";

export function ShelfThumbnail({ shelfNumber, hilight, numberOfBooksToRender = 1000, volumeToFind = null }) {
	let bookNumbers = [];
	const shelfTitle = shelfNumber === 1000 ? "1,000" : shelfNumber;
	for (let i = 1; i <= numberOfBooksToRender; i++) {
		bookNumbers.push(i);
	}
	const bookComponents = bookNumbers.map((bookNumber) => (
		<ThumbnailBookSpine key={bookNumber} bookNumber={bookNumber} />
	));

	return (
		<Link href={volumeToFind ? `/shelf/${shelfNumber}#${volumeToFind}` : `/shelf/${shelfNumber}`}>
			<div className={`thumbnail-shelf${hilight ? " hilighted" : ""}`}>
				<div className="thumbnail-shelf-number-div">{shelfTitle}</div>
				<div className="thumbnail-shelf-spines-div">{bookComponents}</div>
			</div>
		</Link>
	);
}

export function Shelf({ shelfNumber, scrolling, numberOfBooksToRender = 1000 }) {
	let bookNumbers = [];
	const shelfTitle = shelfNumber === 1000 ? "1,000" : shelfNumber;
	for (let i = 1; i <= numberOfBooksToRender; i++) {
		bookNumbers.push(i);
	}
	const bookComponents = bookNumbers.map((bookNumber) => (
		<BookSpine key={bookNumber} bookNumber={bookNumber} scrolling={scrolling} />
	));
	return (
		<div className="shelf">
			<div className="shelf-number-div">{shelfTitle}</div>
			<div className="shelf-spines-div">{bookComponents}</div>
		</div>
	);
}
