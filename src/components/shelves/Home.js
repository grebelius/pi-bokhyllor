import React, { useState, useRef, useLayoutEffect } from "react";
import { VirtualThousandShelves } from "./VirtualThousandShelves";
import "./Home.css";

export default function Home() {
	const [formValue, setFormValue] = useState(1);

	const handleFormInputChange = (e) => {
		e.preventDefault();
		setFormValue(e.target.value);
	};
	const handleFormSubmit = (e) => {
		e.preventDefault();
		const volumeNumber = Math.ceil(formValue / 1000);
		console.log(volumeNumber)
	};

	const inputElement = useRef();

	useLayoutEffect(() => {
		inputElement.current.focus();
	}, []);
	return (
		<>
			<header>
					<h1>One Trillion Digits of Pi</h1>
					<p>One thousand shelves, each containing one thousand volumes</p>
				<form onSubmit={handleFormSubmit}>
					<label>
						Read volume number&nbsp;
						<input
							ref={inputElement}
							type="number"
							min="1"
							max="1000000"
							value={formValue}
							onChange={handleFormInputChange}
						/>
					</label>&nbsp;directly or browse the library below
				</form>
			</header>
			<main>
				<VirtualThousandShelves />
			</main>
		</>
	);
}
