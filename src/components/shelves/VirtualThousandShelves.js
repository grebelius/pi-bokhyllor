import React, { useState, useEffect, useRef, useLayoutEffect } from "react";
import { ShelfThumbnail } from "./Shelf";
import "./VirtualThousandShelves.css";

export const VirtualThousandShelves = ({ scrollToShelf = null, volumeToFind = null }) => {
	const startIndex = 1;
	const itemHeight = 100;
	const tolerance = 20;

	const bookHeight = 90;
	const bookWidth = 12;

	let virtualScrollerElement = useRef(null);

	let [windowScrollOffset, setWindowScrollOffset] = useState(0);
	let [viewportHeight, setViewportHeight] = useState(window.innerHeight);
	let [scrollerWidth, setScrollerWidth] = useState(window.innerWidth);
	let [numberOfBufferedItems, setNumberOfBufferedItems] = useState(
		Math.ceil(viewportHeight / itemHeight) + 2 * tolerance
	);
	let [numberOfBooksToRender, setNumberOfBooksToRender] = useState(scrollerWidth / (itemHeight * 0.12));
	let [totalHeight] = useState(1000 * itemHeight);
	let [toleranceHeight] = useState(tolerance * itemHeight);
	let [itemsAbove] = useState(startIndex - tolerance - 1);
	let [topPaddingHeight, setTopPaddingHeight] = useState(itemsAbove * itemHeight);
	let [bottomPaddingHeight, setBottomPaddingHeight] = useState(totalHeight - topPaddingHeight);
	let [scrolling, setScrolling] = useState(false);
	let [data, setData] = useState([]);

	useLayoutEffect(() => {
		document.body.style.setProperty("overflow-x", "hidden");
		return () => {
			document.body.style.removeProperty("overflow-x");
		};
	}, []);
	useLayoutEffect(() => {
		let resizeTimeoutId = null;
		let scrollTimeoutId = null;
		const deferResizeListenerTime = 150;
		const deferWindowScrollListenerTime = 0;

		const windowResizeListener = () => {
			clearTimeout(resizeTimeoutId);
			resizeTimeoutId = setTimeout(() => {
				setViewportHeight(window.innerHeight);
				setScrollerWidth(virtualScrollerElement.current.offsetWidth);
			}, deferResizeListenerTime);
		};
		const windowScrollListener = () => {
			clearTimeout(scrollTimeoutId);
			setScrolling(true);
			scrollTimeoutId = setTimeout(() => {
				setWindowScrollOffset(window.pageYOffset);
				setScrolling(false);
			}, deferWindowScrollListenerTime);
		};

		window.addEventListener("resize", windowResizeListener);
		window.addEventListener("scroll", windowScrollListener);

		return () => {
			window.removeEventListener("resize", windowResizeListener);
			window.removeEventListener("scroll", windowScrollListener);
		};
	}, []);

	// Trigger scroll animation when scrollToShelf changes
	useEffect(() => {
		if (scrollToShelf === null) return;
		const scrollToTopOffset = (scrollToShelf - 1) * itemHeight;
		window.scrollTo({
			top: scrollToTopOffset,
			left: 0,
			behavior: "smooth",
		});
	}, [scrollToShelf, itemHeight]);
	// Update numberOfBufferedItems when viewportHeight changes
	useEffect(() => {
		setNumberOfBufferedItems(Math.ceil(viewportHeight / itemHeight) + 2 * tolerance);
	}, [viewportHeight, itemHeight, tolerance]);
	// Update numberOfBooksToRender when scroll listener fires
	useLayoutEffect(() => {
		setNumberOfBooksToRender(Math.floor(scrollerWidth / (itemHeight * 0.12)) - 1);
	}, [scrollerWidth, itemHeight]);
	// Update stuff when windowScrollOffset changes
	useEffect(() => {
		const calculateData = (offset, limit) => {
			const data = [];
			const start = Math.max(1, offset);
			const end = Math.min(offset + limit - 1, 1000);
			if (start <= end) {
				for (let i = start; i <= end; i++) {
					data.push(i);
				}
			}
			return data;
		};
		const index = 1 + Math.floor((windowScrollOffset - toleranceHeight) / itemHeight);
		const data = calculateData(index, numberOfBufferedItems);
		const topPadding = Math.max((index - 1) * itemHeight, 0);
		const bottomPadding = Math.max(totalHeight - topPadding - data.length * itemHeight, 0);

		setData(data);
		setTopPaddingHeight(topPadding);
		setBottomPaddingHeight(bottomPadding);
	}, [windowScrollOffset, itemHeight, numberOfBufferedItems, toleranceHeight, totalHeight]);

	// *******************************************************************************************

	let shelfComponents = data.map((shelfNumber) => (
		<ShelfThumbnail
			volumeToFind={scrollToShelf === shelfNumber ? volumeToFind : null}
			hilight={scrollToShelf === shelfNumber}
			key={shelfNumber}
			shelfNumber={shelfNumber}
			scrolling={scrolling}
			numberOfBooksToRender={numberOfBooksToRender}
		/>
	));

	return (
		<div
			ref={virtualScrollerElement}
			className="virtual-scroller"
			style={{ "--book-height": `${bookHeight}px`, "--book-width": `${bookWidth}px` }}
		>
			<div style={{ height: `${topPaddingHeight}px` }} />
			{shelfComponents}
			<div style={{ height: `${bottomPaddingHeight}px` }} />
		</div>
	);
};
