import React, { createContext, useContext, useEffect, createRef } from "react";
import ReactDOM from "react-dom";
import { motion } from "framer-motion";

import "./modal.css";

const modalContext = createContext();

export default function Modal({ children, onModalClose }) {
	useEffect(() => {
		const handleTabKey = (e) => {
			const focusableModalElements = modalRef.current.querySelectorAll(
				'a[href], button, textarea, input[type="text"], input[type="radio"], input[type="checkbox"], select'
			);
			const firstElement = focusableModalElements[0];
			const lastElement = focusableModalElements[focusableModalElements.length - 1];

			if (!e.shiftKey && document.activeElement === lastElement) {
				firstElement.focus();
				return e.preventDefault();
			}
			if (e.shiftKey && document.activeElement === firstElement) {
				lastElement.focus();
				return e.preventDefault();
			}
		};
		const keyListenersMap = new Map([
			[27, onModalClose],
			[9, handleTabKey],
		]);
		function keyListener(e) {
			const listener = keyListenersMap.get(e.keyCode);
			return listener && listener(e);
		}
		document.addEventListener("keydown", keyListener);
		return () => {
			document.removeEventListener("keydown", keyListener);
		};
	});

	const modalRef = createRef();
	const variants = {
		hidden: { opacity: 0, y: "100vh", rotate: 2 },
		visible: { opacity: 1, y: 0, rotate: 0 },
	};

	return ReactDOM.createPortal(
		<div className="modal-container" role="dialog" aria-modal="true" onClick={onModalClose}>
			<motion.div initial="hidden" animate="visible" variants={variants} className="modal-content" ref={modalRef}>
				<modalContext.Provider value={{ onModalClose }}>{children}</modalContext.Provider>
			</motion.div>
		</div>,
		document.body
	);
}

Modal.Header = function ModalHeader(props) {
	const { onModalClose } = useContext(modalContext);

	return (
		<div className="modal-header">
			{props.children}
			<button className="cross-btn" title="Close modal" onClick={onModalClose}>
				x
			</button>
		</div>
	);
};

Modal.Body = function ModalBody(props) {
	return <div className="modal-body">{props.children}</div>;
};

Modal.Footer = function ModalFooter(props) {
	return <div className="modal-footer">{props.children}</div>;
};

Modal.Footer.CloseBtn = function CloseBtn(props) {
	const { onModalClose } = useContext(modalContext);
	return <button {...props} className="close-btn" title="Close modal" onClick={onModalClose} />;
};
