import React, { useLayoutEffect } from "react";
import { Route } from "wouter";

import useCurrentViewportDimensions from "./helpers/viewportDimensions";

import Home from "./components/shelves/Home";
import { VirtualThousandBooks } from "./components/shelves/VirtualThousandBooks";

function App() {
	let { viewportWidth, viewportHeight } = useCurrentViewportDimensions();
	useLayoutEffect(() => {
		document.body.style.setProperty("--vw", viewportWidth);
		document.body.style.setProperty("--vh", viewportHeight);
	}, [viewportWidth, viewportHeight]);

	return (
        <>
            <Route path="/" component={Home} />
            <Route path="/shelf/:shelfNumber">
                {params => <VirtualThousandBooks shelfNumber={params.shelfNumber} />}
            </Route>
        </>
	);
}

export default App;
